// This code will hel us access contents of epress module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create and app/server
// We store our express module to a variable so we could easily access its keywords, functions, and methods.

const express = require("express");

// This code creates an application using express (a.k.a express application)
	// App is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// So the app can read json data
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the usrl can only be received as string or an array
// By applying the option of "extended:true" this allows us to receive infromation to other data types, such as, an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{

	// This is the response that we will expect to receive if the GET method with the right endpoint is successful
	response.send("GET method success. \nHello from /hello endpoint!");
});

app.post("/hello", (request, response)=>{

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


let users = [];

app.post("/signup", (request, response) => {
	if(request.body.userName !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.userName} successfully registered`);
		
		//only one response can be used/display
	}
	else{
		response.send("Please input BOTH username and password");
	}
});

// We use put method to update a document
app.put("/change-password", (request, response) => {
	// a variable for stosring the response
	let message;

	//to check if the user is existing or not in the array
	for(let i=0; i < users.length; i++){
		if(request.body.userName == users[i].userName){
			
			// update an element's object password based on the input on the body
			users[i].password = request.body.password
			message = `user ${request.body.userName}'s has been updated`;
			break;
		}
		else{
			message = "User does not exist.";
		}
	}
	console.log(users);
	response.send(message);
});


// ACTIVITY



// 1
// Create a route that expect GET request at the uri/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
// Code below...

app.get("/home", (request, response)=>{
		response.send("Welcome to the homepage!");
});



// 2
// Create a reoute that expects GET request at the uri/endpoint "/users"
// Response that will be received is a collection of users

app.get("/users", (request, response)=>{
		response.send(users);
});



// 3 
// Create a route that expects DELETE request at the uri/endpoint "/delete-user"
// The program should check if the user is existing before deleting
// Response that will be received is the updated collection of users

app.delete("/delete-user", (request, response)=>{
            
    for(let i=0; i < users.length; i++){
		if(request.body.userName == users[i].userName){
    		users.splice(i, 1);
        	response.send(users);
        }
    	else{
        	response.send("User does not exist");
    	}
    }
         
});


// 4
// Save all the successful request tabs and export collections
// Upload/update yoru gitlab repo and link it to Boodle




// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal

app.listen(port, () => console.log(`Server running at port ${port}`));